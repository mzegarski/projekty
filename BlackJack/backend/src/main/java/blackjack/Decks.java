package blackjack;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Decks {

    private List<Card> decks = new ArrayList<>();

    public Decks(int numberOfDecks) {
        Deck deck = new Deck();
        for (int i = 0; i < numberOfDecks; i++)
            decks.addAll(deck.get());
    }

    public void shuffle() {

        List<Card> tempDecks = new ArrayList<>();

        do {
            int random = new Random().nextInt(decks.size());
            tempDecks.add(decks.get(random));
            decks.remove(random);
        } while (decks.size() != 0);

        decks = tempDecks;
    }

    public Card getCardFromTheTop() {

        Card card = decks.get(0);
        decks.remove(0);
        return card;
    }

    public int size() {

        return decks.size();

    }

    public void add(Card card) {
        decks.add(card);

    }
}