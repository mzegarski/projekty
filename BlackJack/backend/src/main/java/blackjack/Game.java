package blackjack;

import java.util.*;

/**
 * Created by zegarski on 2017-07-28.
 */
public class Game {

    private Player player = new Player();
    private Decks decks = new Decks(8);

    private Card hiddenCroupierCard;
    private List<Card> croupierCards = new ArrayList<>();

    private List<Card> graveyard = new ArrayList<>();
    private double bet = 0;
    private boolean lostGame = false;
    private boolean endGame = false;
    private boolean again = false;

    public Player getPlayer() {

        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Card getHiddenCroupierCard() {
        return hiddenCroupierCard;
    }

    public List<Card> getCroupierCards() {
        return croupierCards;
    }

    public boolean isLostGame() {
        return lostGame;
    }

    public void setBet(double bet) {
        this.bet = bet;
    }

    public void prepareGame() {

        decks.shuffle();

        if (hiddenCroupierCard != null || croupierCards.size()!= 0){
            croupierCards.add(hiddenCroupierCard);
            hiddenCroupierCard = null;
            moveUsedCardsToGraveYard();

        }

        lostGame = false;
        endGame = false;
        again = false;
    }

    public void moveUsedCardsToGraveYard() {

        graveyard.addAll(croupierCards);
        graveyard.addAll(player.getCards());
        croupierCards.clear();
        player.removeCards();
    }

    public void stand() {
        endGame = true;

        List<Card> tempCroupierCards = new ArrayList<>();

        tempCroupierCards.add(hiddenCroupierCard);
        tempCroupierCards.addAll(croupierCards);

        croupierCards = tempCroupierCards;

        croupierCardsValue();

        while (croupierCardsValue() < 17)
            giveCroupierVisibleCard();

                if (croupierCardsValue() <= 21 && croupierCardsValue() >= player.cardsValue())
            lostGame = true;
    }

    public void hit() {
        givePlayerCard();
            if (player.cardsValue() > 21) {
            lostGame = true;
            endGame = true;
        }
    }

    public void dealCards() {
        giveCroupierHiddenCard();
        giveCroupierVisibleCard();
        givePlayerCard();
        givePlayerCard();
    }

    public void givePlayerCard() {
        ifDecksAreEmptyShuffle();
        player.giveCard(decks.getCardFromTheTop());
    }

    public void giveCroupierVisibleCard() {
        ifDecksAreEmptyShuffle();
        croupierCards.add(decks.getCardFromTheTop());
    }

    public void giveCroupierHiddenCard() {
        ifDecksAreEmptyShuffle();
        hiddenCroupierCard = decks.getCardFromTheTop();
    }

    public void ifDecksAreEmptyShuffle() {
        if (decks.size() == 0) {
            do {
                int random = new Random().nextInt(graveyard.size());
                decks.add(graveyard.get(random));
                graveyard.remove(random);

            } while (graveyard.size() != 0);

            decks.shuffle();
        }
    }

    public int croupierCardsValue() {
        int totalValue = 0;
        for (Card c : croupierCards)
            totalValue += c.getValue();
        return totalValue;
    }

}