package blackjack.controller;


import blackjack.entity.Player;
import blackjack.repository.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/player")
public class PlayerController {

    @Autowired
    private PlayerRepository playerRepository;

    @RequestMapping("/add")
    public Player add(@RequestParam(name = "name") String name,
                      @RequestParam(name = "money") double money) {
        Player p = new Player();
        p.setName(name);
        p.setMoney(money);
        return playerRepository.save(p);
    }

    @RequestMapping("/show")
    public List<Player> showAll() {
        return (List<Player>) playerRepository.findAll();
    }

    @RequestMapping("/show/{id}")
    public Player showById(@PathVariable(name = "id") String id) {
        long myId = Long.valueOf(id);
        return playerRepository.findOne(myId);
    }

    @RequestMapping("/edit")
    public void editPlayer(@RequestParam(name = "id") long id, @RequestParam(name = "name") String name, @RequestParam(name = "money") double money) {

        Player p = playerRepository.findOne(id);

        p.setName(name);
        p.setMoney(money);

        playerRepository.save(p);
    }
}
