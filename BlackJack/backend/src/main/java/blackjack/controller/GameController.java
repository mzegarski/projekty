package blackjack.controller;

import blackjack.Card;
import blackjack.Game;
import blackjack.Player;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/game")
public class GameController {

    private Game game = new Game();

    Integer activePlayerId;

    @RequestMapping("/setplayer")
    public void setPlayer(@RequestParam(name = "id") Integer id, @RequestParam(name = "name") String name, @RequestParam(name = "money") double money) {

        activePlayerId = id;

        game.setPlayer(new Player(name, money));

    }

    @RequestMapping("/getplayer")
    public Player getPlayer() {

        return game.getPlayer();
    }

    @RequestMapping("/getplayerid")
    public int getActivePlayerId(){
        return activePlayerId;
    }

    @RequestMapping("/bet")
    public void bet(@RequestParam(name = "bet") double bet) {


        game.moveUsedCardsToGraveYard();
        game.prepareGame();

        game.setBet(bet);
        game.dealCards();
    }

    @RequestMapping("/hit")
    public boolean hit() {
        game.hit();
        return game.isLostGame();
    }

    @RequestMapping("/stand")
    public boolean stand() {
        game.stand();
        return game.isLostGame();
    }

    @RequestMapping("/croupiercards")
    public List<Card> croupierCards() {
        return game.getCroupierCards();
    }

    @RequestMapping("/croupierhidderncard")
    public Card croupierHiddenCard() {
        return game.getHiddenCroupierCard();
    }

    @RequestMapping("/playercards")
    public List<Card> playerCards() {


        return game.getPlayer().getCards();
    }

    @RequestMapping("/double")
    public void setDoubleBet(@RequestParam(name = "bet") double bet){

        game.setBet(bet);
    }
}