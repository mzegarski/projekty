package blackjack;

import java.util.ArrayList;
import java.util.List;

public class Player {

    private String name;
    private double money;
    private List<Card> cards = new ArrayList<>();

    public Player() {
    }

    public Player(String name, double money) {
        this.name = name;
        this.money = money;
    }

    public void giveCard(Card card) {
        cards.add(card);
    }

    public List<Card> getCards() {
        return cards;
    }

    public void removeCards() {
        cards.clear();
    }

    public String getName() {
        return name;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    public int cardsValue() {

        int value = 0;
        for (Card c : cards)
            value += c.getValue();
        return value;

    }

    public void printCards() {

        for (Card c : cards)
            System.out.println(c.getRank() + " of " + c.getColor() + " [" + c.getValue() + "]");

    }
}