package blackjack;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by zegarski on 2017-07-28.
 */
public class Deck {

    private List<Card> deck = new ArrayList<>();

    public Deck() {
        create();
    }

    private void create() {

        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 13; j++)
                deck.add(new Card(i, j));
        }
    }

    public List<Card> get() {
        return deck;
    }

    public void printAll() {
        for (Card c : deck) {
            System.out.println(c.getRank() + " of " + c.getColor() + " [" + c.getValue() + "]");
        }
    }

    public void printCard(int id) {

        Card c = deck.get(id);
        System.out.println(c.getRank() + " of " + c.getColor());

    }

    public void shuffle() {

        List<Card> tempDeck = new ArrayList<>();

        for (int i = 0; i < 52; i++) {
            int random = new Random().nextInt(deck.size());
            tempDeck.add(i, deck.get(random));
            deck.remove(random);
        }
        deck = tempDeck;
    }
}
