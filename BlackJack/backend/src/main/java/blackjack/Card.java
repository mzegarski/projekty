package blackjack;

/**
 * Created by zegarski on 2017-07-28.
 */
public class Card {

    private String[] colors = {"Clubs", "Diamonds", "Hearts", "Spades"};
    private String[] ranks = {"Two", "Three", "Four", "Five",
            "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King", "Ace"};

    private String color;
    private String rank;
    private int value;

    public Card(int color, int rank) {
        this.color = colors[color];
        this.rank = ranks[rank];

        if (rank <= 7)
            this.value = rank + 2;
        else if (rank <= 11)
            this.value = 10;
        if (rank == 12)
            this.value = 11;
    }

    public String getColor() {
        return color;
    }

    public String getRank() {
        return rank;
    }

    public int getValue() {
        return value;
    }
}