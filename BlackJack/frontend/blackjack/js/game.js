var app = angular.module('game', []);

var btnbet = document.getElementById('betbtn');
var betv = document.getElementById('betvalue');
var btnhit = document.getElementById('hit');
var btnstand = document.getElementById('stand');
var btndouble = document.getElementById('double');
var cons = document.getElementById('console');
var btnexit = document.getElementById('exit');
var cvalue = document.getElementById('cvalue');
var pvalue = document.getElementById('pvalue');
var chiddencard = document.getElementById('chiddencard');
var fillercard1 = document.getElementById('fillercard1');
var fillercard2 = document.getElementById('fillercard2');
var fillercard3 = document.getElementById('fillercard3');
var x = Number(0);
var pname = document.getElementById('pname');
var pmoney = document.getElementById('pmoney');
var logs = new Array;
logs[0] = "Witaj";

var url = 'http://localhost:8080/game/';

app.controller('gameController', function ($scope, $http) {

	$http({
		url: url + 'getplayerid',
		dataType: 'json'
	}).then(function (success) {
		$scope.getplayerid = success.data;

	}, function (error) {
		console.error(error);
	});

	$http({
		url: url + 'getplayer',
		dataType: 'json'
	}).then(function (success) {
		$scope.player = success.data;

		pname.value = $scope.player.name;
		pmoney.value = $scope.player.money;

	}, function (error) {
		console.error(error);
	});

	$scope.croupiercards = function () {

		$http({
			url: url + 'croupiercards',
			dataType: 'json'
		}).then(function (success) {
			$scope.ccards = success.data;
			cvalue.value = Number(0);
			for (let i in $scope.ccards) {
				$scope.ccards[i].image = './resources/cards/' + $scope.ccards[i].rank + "Of" + $scope.ccards[i].color + ".png";
				cvalue.value = Number(cvalue.value) + Number($scope.ccards[i].value);
			}
		}, function (error) {
			console.error(error);
		});
	}

	$scope.editplayer = function (id, name, money) {

		$http({
			url: 'http://localhost:8080/player/edit',
			dataType: 'json',
			params: {
				id: id,
				name: name,
				money: money
			}
		}).then(function (succ) {
			$scope.editedplayer = succ.data;

		}, function (err) {
			console.error(err);
		});
	}

	$scope.playercards = function () {
		$http({
			url: url + 'playercards',
			dataType: 'json'
		}).then(function (success) {
			$scope.pcards = success.data;
			pvalue.value = Number(0);
			for (let i in $scope.pcards) {
				$scope.pcards[i].image = './resources/cards/' + $scope.pcards[i].rank + "Of" + $scope.pcards[i].color + ".png";
				pvalue.value = Number(pvalue.value) + Number($scope.pcards[i].value);
			}
		}, function (error) {
			console.error(error);
		});
	}

	$scope.croupierhiddencard = function () {
		$http({
			url: url + 'croupierhidderncard',
			dataType: 'json'
		}).then(function (success) {
			$scope.chcard = success.data;

		}, function (error) {
			console.error(error);
		});
	}

	$scope.bet = function (bet) {
		$http({
			url: url + 'bet',
			method: 'POST',
			dataType: 'json',
			params: {
				bet: bet
			}
		}).then(function (succ) {
			$scope.game = succ.data;

			$scope.croupiercards();
			$scope.croupierhiddencard();
			$scope.playercards();

		}, function (err) {
			console.error(err);
		});
	}

	$scope.startgame = function () {

		if (pmoney.value - betv.value >= 0) {

			if (betv.value > 0) {

				if (x == 0) {
					fillercard1.parentNode.removeChild(fillercard1);
					fillercard2.parentNode.removeChild(fillercard2);
					fillercard3.parentNode.removeChild(fillercard3);
					x++;
				}

				chiddencard.style.visibility = 'visible';
				logs.push("Przyjmuję zakład");
				$scope.message = logs;
				btnbet.disabled = true;
				betv.disabled = true;
				btnhit.disabled = false;
				btnstand.disabled = false;
				btndouble.disabled = false;
				$scope.bet(betv.value);

				pmoney.value = pmoney.value - betv.value;
				$scope.editplayer($scope.getplayerid, pname.value, pmoney.value);

			} else {
				alert("Należy wpisać wartość zakładu większą od 0");
			}

		} else {
			alert("Nie masz tyle pieniedzy");
		}
	}

	$scope.stopgame = function () {

		btnbet.disabled = false;
		betv.disabled = false;
		betv.value = 0;
		btnhit.disabled = true;
		btnstand.disabled = true;
		btndouble.disabled = true;
		var newlog = ["Nowa gra!"];
		logs = newlog;
	}

	$scope.hit = function () {
		btndouble.disabled = true;
		logs.push("Daje karte");
		$scope.message = logs;

		$http({
			url: url + 'hit',
			dataType: 'json'
		}).then(function (success) {
			$scope.gameresult = success.data;

			$scope.playercards();

			if ($scope.gameresult == true) {
				logs.push("Koniec gry: przegrana");
				$scope.message = logs;
				$scope.stopgame();
			}

		}, function (error) {
			console.error(error);
		});
	}

	$scope.stand = function () {

		chiddencard.style.visibility = 'hidden';
		$http({
			url: url + 'stand',
			dataType: 'json'
		}).then(function (success) {
			$scope.gameresult = success.data;

			$scope.croupiercards();
			$scope.playercards();

			if ($scope.gameresult == true) {
				logs.push("Koniec gry: przegrana");
				$scope.message = logs;
				$scope.stopgame();
			} else {

				pmoney.value = parseFloat(pmoney.value) + parseFloat(betv.value * 1.5);

				$scope.editplayer($scope.getplayerid, pname.value, pmoney.value);

				logs.push("Koniec gry: wygrana");
				$scope.message = logs;
				$scope.stopgame();
			}

		}, function (error) {
			console.error(error);
		});
	}

	$scope.double = function () {

		if (pmoney.value - betv.value * 2 >= 0) {

			logs.push("Podwajam stawkę!")
			btndouble.disabled = true;
			$scope.message = logs;

			pmoney.value = pmoney.value - betv.value;
			betv.value = betv.value * 2;

			$scope.editplayer($scope.getplayerid, pname.value, pmoney.value);
			$http({
				url: url + 'double',
				method: 'POST',
				dataType: 'json',
				params: {
					bet: betv.value
				}
			}).then(function (succ) {
				$scope.game = succ.data;
			}, function (err) {
				console.error(err);
			});
		} else {
			alert("Nie masz tyle pieniedzy");
		}
	}
	$scope.exit = function () {
		alert("Dowidzenia");
	}
});
