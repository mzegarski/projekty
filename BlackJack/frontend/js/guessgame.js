var tries = document.getElementById('tries');
var min = document.getElementById('min');
var max = document.getElementById('max');
var hints = document.getElementById('hints');
var cheat = document.getElementById('cheat');
var btnstart = document.getElementById('start');
var btnstop = document.getElementById('stop');
var typeguess = document.getElementById('typeguess');
var btnguess = document.getElementById('guess');
var generatedNumber;
var triestext = document.getElementsByClassName('triestext')[0];
var triesCopy;
var num = document.getElementsByClassName('number')[0];
var hinttext = document.getElementsByClassName('hinttext')[0];

var Play = function () {

	this.generateNumber = function () {
		return Math.floor(Math.random() * (max.value - min.value + 1)) + parseInt(min.value);
	};
};

var game = new Play();

var startgame = function () {

	tries.disabled = true;
	min.disabled = true;
	max.disabled = true;
	hints.disabled = true;
	cheat.disabled = true;
	btnstart.disabled = true;
	typeguess.removeAttribute('disabled');
	btnguess.removeAttribute('disabled');
	generatedNumber = game.generateNumber();
	if (cheat.checked == true) {
		num.innerHTML = "Tajna liczba to: " + generatedNumber;
	}
	triesCopy = tries.value;
	triestext.innerHTML = "Liczba prób: " + triesCopy;
	typeguess.value = "";

};

var stopgame = function () {

	tries.removeAttribute('disabled');
	min.removeAttribute('disabled');
	max.removeAttribute('disabled');
	hints.removeAttribute('disabled');
	cheat.removeAttribute('disabled');
	btnstart.removeAttribute('disabled');
	typeguess.disabled = true;
	btnguess.disabled = true;
	triestext.innerHTML = "";
	hinttext.innerHTML = "";
	num.innerHTML = "";

};

var hinter = function () {
	if (typeguess.value > generatedNumber)
		hinttext.innerHTML = "Za dużo";
	else
		hinttext.innerHTML = "Za mało";
}

btnstart.addEventListener('click', function (e) {

	if ((max.value - min.value) < 0) {
		alert("Źle wpisany zakres");
	} else {
		startgame();
	}

});

btnstop.addEventListener('click', function (e) {
	stopgame();
});

btnguess.addEventListener('click', function (e) {

	if (typeguess.value == generatedNumber) {
		stopgame();
		triestext.innerHTML = "WINNER!";
	} else {
		triesCopy--;
		if (triesCopy == 0) {
			stopgame();
			triestext.innerHTML = "Looooser";
		} else {
			triestext.innerHTML = "Liczba prób: " + triesCopy;

			if (hints.checked == true) {
				hinter();
			}

		}
	}
});
