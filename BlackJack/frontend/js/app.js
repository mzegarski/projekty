var app = angular.module('RESTApp', ['ngRoute']);
var url = 'http://localhost:8080/';

app.config(function ($routeProvider) {
	var path = './views/';
	$routeProvider
		.when('/', {
			templateUrl: path + 'main.html'
		})
		.when('/blackjack', {
			templateUrl: path + 'blackjack.html',
			controller: 'blackjackController'
		})
		.when('/guessgame', {
			templateUrl: path + 'guessgame.html',
		})
		.when('/rules', {
			templateUrl: path + 'rules.html',
		})
});

app.controller('blackjackController', function ($scope, $http) {
	$http({
		url: url + 'player/show',
		dataType: 'json'
	}).then(function (succ) {
		$scope.players = succ.data;
	}, function (err) {
		console.error(err);
	});

	$scope.add = function (name, money) {
		$http({
			url: url + 'player/add',
			method: 'GET',
			dataType: 'json',
			params: {
				name: name,
				money: money
			}
		}).then(function (success) {
			console.log(success);
			$scope.message = "Dodano poprawnie gracza.";
			$scope.players.push(success.data);
		}, function (error) {
			console.error(error);
		});
	};

	$scope.new = function () {
		var newplayer = prompt("Podaj nazwę gracza: ", "Player");

		if (newplayer == null || newplayer == "") {
			alert("Anulowano.")
		} else {
			$scope.add(newplayer, 1000);

		}
	};

	$scope.playerid = function () {
		var pid = Number(prompt("Wybierz gracza po ID: "));

		if (pid == null || pid == "") {
			alert("Anulowano.")
		} else if (pid !== parseInt(pid, 10)) {
			alert("Należy wpisać numer ID z listy.")
		} else {
			$http({
				url: url + 'player/show/' + pid,
				dataType: 'json'
			}).then(function (succ) {
				$scope.playerbyid = succ.data;
				if ($scope.playerbyid == "") {
					alert("Nie ma takiego gracza")
				} else {

					$http({
						url: url + '/game/setplayer',
						method: 'POST',
						dataType: 'json',
						params: {
							id: $scope.playerbyid.id,
							name: $scope.playerbyid.name,
							money: $scope.playerbyid.money
						}
					}).then(function (succ) {
						$scope.playeringame = succ.data;

					}, function (err) {
						console.error(err);
					});


					window.location.href = '/blackjack/game.html';
				}

			}, function (err) {
				console.error(err);
			});
		}
	};
});
